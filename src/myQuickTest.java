import java.util.Arrays;
import java.util.Random;

/**
 * Created by Marko on 14-Oct-16.
 */
public class myQuickTest extends QuickSort{

    private int [] initialArray;
    private int [] sortedArray;
    private String initialMessage= "The initial array is:";
    private String finalMessage = "The final sorted array is:";

    private void printArray(int[] array){
        System.out.println(Arrays.toString(array));
    }
    private void printMessage(String message){
        System.out.println(message);
    }
    private int InitialArraySize(){
        return initialArray.length;
    }


    private int[] generateRandomList() {

        Random rand = new Random();
        int i;
        int [] array = new int[9];

        for (i=0; i<9; i++) {
            int pick = rand.nextInt(100) + 1;
            array[i] = pick;
        }
        return array;
    }

    private int [] generateFixedList(){
        return new int[]{54, 26, 93, 17, 77, 90, 31, 44, 55, 20};
    }


    public static void main(String[] args) {

        myQuickTest myQuickTest = new myQuickTest();
        myQuickTest.initialArray = myQuickTest.generateFixedList();
        myQuickTest.printMessage(myQuickTest.initialMessage);
        myQuickTest.printArray(myQuickTest.initialArray);
        myQuickTest.printMessage(myQuickTest.finalMessage);
        myQuickTest.sortedArray = myQuickTest.quickPartition(myQuickTest.initialArray, 0 , myQuickTest.InitialArraySize() - 1);
        myQuickTest.printArray(myQuickTest.sortedArray);
    }
}
