/**
 * Created by Marko on 14-Oct-16.
 */
class QuickSort {

    /*private void quickSort(int[] array, int low, int high){

        if(low < high) {
            int pivot;

            pivot = quickPartition(array, low, high);
            quickSort(array, low, pivot - 1);
            quickSort(array, pivot + 1, high);
        }
    }*/

     int [] quickPartition(int[] array, int low, int high){

        int  pivot = array[low], end_mark = high, start_mark = 1, temp, temp1;
        boolean checked = false;

         if(start_mark == end_mark){
             checked = true;
         }

        while (start_mark != end_mark) {
            if (array[start_mark] > pivot) {
                while (end_mark != start_mark) {
                    if (array[end_mark] < pivot) {
                        temp = array[end_mark];
                        array[end_mark] = array[start_mark];
                        array[start_mark] = temp;

                        break;
                    }
                    end_mark--;
                }
            }
            start_mark++;
        }

        return array;
    }

}


